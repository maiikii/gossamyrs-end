var express = require('express');
const flash = require('express-flash');
const session = require('express-session');
var consolidate = require('consolidate');
const cookieparser = require('cookie-parser');
var bodyparser = require('body-parser');
const database = require('./database');
const User = require('./models').User;
const ReadList = require('./models').ReadList;

var app = express();

app.set('views', './views');
app.engine('html', consolidate.nunjucks);

app.use(bodyparser.urlencoded({ extended: false }));
app.use(cookieparser('secret-cookie'));
app.use(session({ resave: false, saveUninitialized: false, secret: 'secret-cookie' }));
app.use(flash());

app.use('/uploads', express.static('./uploads'));
app.use(express.static('./static'));
app.use(require('./routes/auth'));
app.use(require('./routes/twitter'));
app.use(require('./routes/google'));

function retrieveUser(req,res,next) {
    req.user = req.session.currentUser;
	next();
};

app.get('/', retrieveUser, function(request, response) {
	console.log(request.session.currentUser + " " + request.user);
	if (request.user) {
        ReadList.findAll({ where: { type: 'manga', userId: request.user.id } }).then(function(manga) {
			ReadList.findAll({ where: { type: 'webtoon', userId: request.user.id } }).then(function(webtoon) {
				ReadList.findAll({ where: { type: 'novel', userId: request.user.id } }).then(function(novel) {
					response.render('index.html', {
						loggedIn: true,
						user: request.user,
						manga: manga,
						webtoon: webtoon,
						novel: novel
					});
				});
			});
		});
    }else {
    	response.render('index.html', {
			loggedIn: false,
			manga: null,
			webtoon: null,
			novel: null
		});
    }
});

app.get('/add-entry', requireSignedIn, function(request, response) {
  response.render('add-entry.html');
});

app.get('/login', function(request, response) {
  response.render('login.html');
});

function requireSignedIn(request, response, next) {
    if (!request.session.currentUser) {
        return response.redirect('/login');
    }
    next();
}

app.listen(1102, function() {
  console.log('Server is now listening at port 1102');
});

const Sequelize = require('sequelize');
const database = require('./database');

var User = database.define('user', {
	id: {
		type: Sequelize.STRING,
		unique: true,
		primaryKey: true
	},
	username: {
		type: Sequelize.STRING,
		allowNull: false
	},
	email: {
		type: Sequelize.STRING
	},
	password: {
		type: Sequelize.STRING
	}
}, {
	timestamps: true
});

var ReadList = database.define('list', {
	id: {
		type: Sequelize.STRING,
		unique: true,
		primaryKey: true
	},
	title: {
		type: Sequelize.STRING,
		allowNull: false
	},
	author: {
		type: Sequelize.STRING
	},
	image: {
		type: Sequelize.STRING
	},
	type: {
		type: Sequelize.ENUM('manga', 'webtoon', 'novel'),
		allowNull: false
	}
}, {
	timestamps: true
});

ReadList.belongsTo(User);

var Contact = database.define('contact', {
	id: {
		type: Sequelize.STRING,
		unique: true,
		primaryKey: true
	},
	name: {
		type: Sequelize.STRING,
	},
	email: {
		type: Sequelize.STRING
	},
	message: {
		type: Sequelize.STRING
	}
});

database.sync();


module.exports.User = User;
module.exports.ReadList = ReadList;
module.exports.Contact = Contact;
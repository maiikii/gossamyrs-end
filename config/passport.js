const passport = require('passport');
const TwitterPassport = require('passport-twitter');
const GoogleStrategy = require( 'passport-google-oauth20' ).Strategy;
const User = require('../models').User;
const database = require('../database');

passport.use(new TwitterPassport({
    consumerKey: 'uBLbhOusNT58BggYyYzvL7dqN',
    consumerSecret: '4wWcwGW830wvPcwmxIc8vL3WpAOO7D8qFn1UVXoLY4w9TbhSzc',
    callbackURL: 'http://quinsiren.webeng.life/auth/twitter/callback'
}, function(token, secret, profile, cb) {
    console.log(profile);
    User.findOrCreate({
        where: { id: profile.id, username: profile.username },
        defaults: { password: '' }
    }).then(function(result) {
        cb(null, result[0]);
    });
}));

passport.use(new GoogleStrategy({
    clientID:     '479301319318-r63ika47dl51jkbae35ipqj7ped78ome.apps.googleusercontent.com',
    clientSecret: 'moycXsOxvOaUw-yharC5ez3f',
    callbackURL: "http://quinsiren.webeng.life/auth/google/callback",
    passReqToCallback: true
  },
  function(request, accessToken, refreshToken, profile, done) {
    console.log(profile);
    process.nextTick(function () {
        User.findOrCreate({ 
            where: { id: profile.id, username: profile.displayName, email: profile.email },
            defaults: { password: '' }
        }).then(function(result) {
            return done(null, result[0]);
        });
    });
  }
));

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.findOne({ where: { id: id } }).then(function(user) {
        done(null, user);
    });
});

module.exports = passport;

const express = require('express');
const passport = require('../config/passport');
const router = require('./auth');

router.use(passport.initialize());

router.get('/auth/google',
  passport.authenticate('google', { scope: 
  	[ 'https://www.googleapis.com/auth/plus.login',
  	'https://www.googleapis.com/auth/userinfo.profile',
    'https://www.googleapis.com/auth/userinfo.email',
    'profile', 'email' ] 
}));
 
router.get( '/auth/google/callback', 
    passport.authenticate( 'google', {
        failureRedirect: '/auth/google/failure'
    }),
    function(req, res) {
        req.session.currentUser = req.user;
        res.redirect('/');
    }
);

module.exports = router;

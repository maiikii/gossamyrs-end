const bcrypt = require('bcryptjs');
const express = require('express');
const shortid = require('shortid');
const multer = require('multer');
const database = require('../database');
const User = require('../models').User;
const ReadList = require('../models').ReadList;
const Contact = require('../models').Contact;

const router = new express.Router();

function generateID() {
    var check = false;
    var id;

    do {    
        id = shortid.generate();
        User.findOne({ where: { id: id } }).then(function(user) {
            if(user !== null) {
                check = true;
            }
        }).catch(function(err) {
            console.log("Generated ID error");
        });
    }while(check);

    return id;
}

router.post('/create', function(req, res) {
    const username = req.body.username;
	const email = req.body.email;
    const password = req.body.password;
    const confirmation = req.body.confirmation;

	User.findOne({ where: { email: email } }).then(function(user) {
        if (user !== null) {
            req.flash('loginMessage', 'Email is already in use');
            return res.redirect('/login#');
        }

		if (password !== confirmation) {
            req.flash('loginMessage', 'Passwords do not match');
	        return res.redirect('/login#');
	    }

        User.findOne({ where: { username: username } }).then(function(user) {
            if(user !== null) {
                req.flash('loginMessage', 'Username is already taken');
            return res.redirect('/login#');
            }
        });

        const salt = bcrypt.genSaltSync();
        const hashedPassword = bcrypt.hashSync(password, salt);

        var id = generateID();

        database.transaction(function(t) {
            return User.create({
                id: id,
                username: username,
                email: email,
                password: hashedPassword,
                salt: salt
                }, { transaction: t }
            ).catch(function(err) {
                console.log("Create transaction error");
            });
        }).then(function() {
            res.redirect('/');
        });
    });
});

router.post('/login', function(req, res) {
	const username = req.body.username;
    const password = req.body.password;

	User.findOne({ where: { username: username } }).then(function(user) {
        if (user === null) {
            req.flash('loginMessage', 'Incorrect username');
            return res.redirect('/login');
        }

		const match = bcrypt.compareSync(password, user.password);
		if (!match) {
			req.flash('loginMessage', 'Incorrect password');
			return res.redirect('/login');
		}

        req.session.currentUser = user;
		res.redirect('/');
    });
});

router.get('/logout', function(req, res) {
	req.session.destroy();
	res.redirect('/');
});

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads');
    },
    filename: function(req, file, cb) {
        const ext = file.originalname.split('.').pop();
        const filename = (new Date()).getTime() + '.' + ext;
        cb(null, filename);
    }
});
const upload = multer({ storage: storage });

router.post('/add-entry', upload.single('image'), function(req, res) {
    const title = req.body.title;
    const author = req.body.author;
    const type = req.body.type;
    var currUser = req.session.currentUser;

    console.log(currUser == null);

    User.findOne({ where: { username: currUser.id } }).then(function(user) {
        console.log(user == null);
        ReadList.findAll({ where: { title: title } }).then(function(entry) {
            console.log(entry == null);
            if(entry.title == title && entry.author == author) {
                req.flash('statusMessage', 'Entry already exists');
                res.redirect('/add-entry');
            }

            var id = generateID();

            ReadList.create({
                id: id,
                title: title,
                author: author,
                image: '/uploads/' + req.file.filename,
                type: type,
                userId: currUser.id
            });

            req.flash('statusMessage', 'Entry added successfully');
            res.redirect('/add-entry');
        });
    });
});

router.post('/edit', function(req, res) {
    res.render('edit-entry.html', {
        title: req.body.title,
        author: req.body.author,
        category: req.body.category,
        image: req.body.image,
        id: req.body.id
    });
});

router.post('/edit-entry', upload.single('image'), function(req, res) {
    const id = req.body.id;
    const title = req.body.title;
    const author = req.body.author;
    const type = req.body.type;
    var image = req.body.image;

    if(req.file != null) {
        image = '/uploads/' + req.file.filename;
    }

    ReadList.update(
        {   title: title,
            author: author,
            image: image,
            type: type
        },
        { 
            fields: ['title', 'author', 'image', 'type'],
            where: {id: id}
        }
    );

    req.flash('statusMessage', 'Entry edited successfully');
    res.render('edit-entry.html', {
        title: title,
        author: author,
        category: type,
        image: image,
        id: id
    });
});

router.post('/delete-entry', function(req, res) {
    const id = req.body.id;

    ReadList.destroy({
        where: { id: id }
    }).then(function(rowDeleted){
        if(rowDeleted === 1){
            res.redirect('/');
        }
    }, function(err){
        console.log(err); 
    });
});

router.post('/contact', function(req, res) {
    const name = req.body.name;
    const email = req.body.email;
    const message = req.body.message;

    var id = generateID();

    Contact.create({
        id: id,
        name: name,
        email: email,
        message: message
    });

    res.redirect('/');
});

module.exports = router;